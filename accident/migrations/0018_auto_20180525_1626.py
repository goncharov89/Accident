# Generated by Django 2.0.3 on 2018-05-25 13:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accident', '0017_auto_20180525_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accident',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='accident',
            name='status',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='accident.Status', verbose_name='Статус'),
        ),
        migrations.AlterField(
            model_name='accident',
            name='system',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='accident.System', verbose_name='Система'),
        ),
        migrations.AlterField(
            model_name='events',
            name='accident',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accident.Accident'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='accident',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accident.Accident'),
        ),
    ]

# Generated by Django 2.0.3 on 2018-05-24 12:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accident', '0010_auto_20180524_1501'),
    ]

    operations = [
        migrations.AddField(
            model_name='accident',
            name='source',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='accident.Source'),
        ),
        migrations.AlterField(
            model_name='accident',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='accident.Users'),
        ),
        migrations.AlterField(
            model_name='accident',
            name='status',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='accident.Status'),
        ),
        migrations.AlterField(
            model_name='accident',
            name='system',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='accident.System'),
        ),
        migrations.AlterField(
            model_name='events',
            name='accident',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accident.Accident'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='accident',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accident.Accident'),
        ),
    ]

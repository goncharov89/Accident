# Generated by Django 2.0.3 on 2018-05-23 14:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accident', '0005_auto_20180523_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accident',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accident.Users'),
        ),
        migrations.AlterField(
            model_name='accident',
            name='status',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='accident.Status'),
        ),
        migrations.AlterField(
            model_name='accident',
            name='system',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accident.System'),
        ),
        migrations.AlterField(
            model_name='events',
            name='accident',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accident.Accident'),
        ),
    ]
